import Head from 'next/head'
import React, {useState} from 'react'
import Image from 'next/image'
import Link from 'next/link';
import { strings } from '../constants/constant';




const Sidebar = (props) => {
    const [show, setShow] = useState(false)
    const [arrow, setArrow] = useState({salesArrow: false, adminArrow: false, campaignArrow: false})
    return (
        <div>
            <Head>
                <meta charSet="UTF-8" />
                <title>Dashboard | CodingCRM </title>
                <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet' />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            </Head>
            <div className={`sidebar ${show ? "close" : ""} `}>
                <div className="logo-details">
                    <i className="bx bxl-c-plus-plus"></i>
                    <span className="logo_name">CodingCRM</span>
                </div>
                <ul className="nav-links">
                    <li>
                        <a href="#">
                            <i className="bx bx-grid-alt"></i>
                            <Link href="/dashboard/main"><span className="link_name">Dashboard</span></Link>
                        </a>
                        <ul className="sub-menu blank">
                            <li><Link href="/dashboard/main"><a className="link_name">Dashboard</a></Link></li>
                        </ul>
                    </li>
                    <li className={`${arrow.salesArrow ? "showMenu":""}`}>
                        <div className="iocn-link">
                            <a href="#">
                                <i className="bx bx-collection"></i>
                                <span className="link_name" onClick={()=>{setArrow({...arrow, salesArrow: !arrow.salesArrow})}}>{strings.sales}</span>
                            </a>
                            <i className="bx bxs-chevron-down arrow" onClick={()=>{setArrow({...arrow, salesArrow: !arrow.salesArrow})}}></i>
                        </div>
                        <ul className="sub-menu">
                            <li><a className="link_name" href="#">{strings.sales}</a></li>
                            <li><Link href="/dashboard/sales/customer"><a>Customer</a></Link></li>
                            <li><Link href="/dashboard/sales/lead"><a>Lead</a></Link></li>
                        </ul>
                    </li>
                    <li className={`${arrow.adminArrow ? "showMenu":""}`}>
                        <div className="iocn-link">
                            <a href="#">
                                <i className="bx bx-book-alt"></i>
                                <span className="link_name" onClick={()=>{setArrow({...arrow, adminArrow: !arrow.adminArrow})}}>Admin</span>
                            </a>
                            <i className="bx bxs-chevron-down arrow" onClick={()=>{setArrow({...arrow, adminArrow: !arrow.adminArrow})}}></i>
                        </div>
                        <ul className="sub-menu">
                            <li><a className="link_name" href="#">Admin</a></li>
                            <li><Link href="/dashboard/admin/account"><a>Account</a></Link></li>
                            <li><Link href="/dashboard/admin/master"><a>Master</a></Link></li>
                        </ul>
                    </li>
                    <li className={`${arrow.campaignArrow ? "showMenu":""}`}>
                        <div className="iocn-link">
                            <a href="#">
                                <i className="bx bx-book-alt"></i>
                                <span className="link_name" onClick={()=>{setArrow({...arrow, campaignArrow: !arrow.campaignArrow})}}>Campaign</span>
                            </a>
                            <i className="bx bxs-chevron-down arrow" onClick={()=>{setArrow({...arrow, campaignArrow: !arrow.campaignArrow})}}></i>
                        </div>
                        <ul className="sub-menu ">
                            <li><a className="link_name" href="#">Campaign</a></li>
                            <li><Link href="/dashboard/campaign/whatsapp"><a>WhatsApp</a></Link></li>
                            <li><Link href="/dashboard/campaign/telegram"><a>Telegram</a></Link></li>
                            <li><Link href="/dashboard/campaign/email"><a>Email</a></Link></li>
                            <li><Link href="/dashboard/campaign/sms"><a>SMS</a></Link></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i className="bx bx-line-chart"></i>
                            <Link href="/dashboard/report"><span className="link_name">Report</span></Link>
                        </a>
                        <ul className="sub-menu blank">
                            <li><Link href="/dashboard/report"><a className="link_name">Report</a></Link></li>
                        </ul>
                    </li>
                    <li>
                        <div className="profile-details">
                            <div className="profile-content">
                                {/* <img src="/public/profile.jpg" alt="profileImg" /> */}
                                <Image src="/images/stepo.jpg" width="52" height="52" />
                            </div>
                            <div className="name-job">
                                <div className="profile_name">StepoGrammer</div>
                                <div className="job">Web Desginer</div>
                            </div>
                            <Link href="/dashboard/login"><i className="bx bx-log-out"></i></Link>
                        </div>
                    </li>
                </ul>
            </div>
            <section className="home-section">
                <div className="home-content">
                    <i className="bx bx-menu" onClick={()=>setShow(!show)}></i>
                    <span className="text">{props.title}</span>
                </div>
                    <h6 className="text">{props.textContent}</h6>
            </section>
        </div>
    )
}

export default Sidebar
