import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

const Login = () => {
    return (
        <div>
            <h1 className='mt-5 mb-5 text-center'>CRM-CUSTOMER RELATIONSHIP MANAGEMENT</h1>
            <div className="row">
                <div className="col-sm-6">
                    <div className='loginWall d-none d-lg-block d-sm-none'>
                        <Image src='/images/CRM.jpg' width='800' height='500' />
                    </div>
                </div>
                <div className="col-sm-6">
                    <h1 className="text-center">Memeber Login</h1>
                    <div className="login-form">
                        <form>
                            <div className="mb-3">
                                <label className="form-label">Username</label>
                                <input className="form-control" />
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Password</label>
                                <input type="password" className="form-control" />
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <Link href="/dashboard/main"><a className='btn btn-primary'> Sign-in</a></Link>
                                </div>
                                <div className="col-6">
                                    <div className="have-account">
                                        New User? <Link href="/dashboard/signup"><a>Signup Here!</a></Link>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Login
