import React from 'react'

const SignUp = () => {
    return (
        <div>
            <section className="signup">
                <div className="container mt-5">
                    <div className="signup-content">
                        <h1>Sign Up page</h1>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default SignUp
